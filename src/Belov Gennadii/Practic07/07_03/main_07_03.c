// ��������� ����������� ���� �� ������� ���������� ��������
#include "tab_re.h"
#include <stdio.h>
#include <stdlib.h>
#include <locale.h>

int main(int argc, char *argv[])
{
	setlocale(LC_CTYPE, "RUS");
	if (argc != 2)
	{
		printf("������!\n ������� ������ ���������\n");
		exit(0);
	}
	FILE *fp_in;
	if ((fp_in = fopen(argv[1], "rb")) == NULL)      //  �������� ���� ��� ������
	{
		puts("������ ��� �������� ��������� �����\n");
		exit(0);
	}
	Analysis(fp_in);             // ������ ��������� ����� � ���������� ���������

	if (fclose(fp_in) != 0)
		puts("������ ��� �������� ������ ���������\n");
	return 0;
}