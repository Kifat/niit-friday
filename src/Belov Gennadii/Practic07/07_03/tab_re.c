#include "tab_re.h"
#include <stdio.h>
#include <stdlib.h>
#include <locale.h>
#define M 256
#define N 71
void Analysis(FILE *fp_in)
{
	int symb[M] = { 0 };     /*������� ������� ������������� ������� �� ������� ASCII
							 �������� � ������� ����� ���-�� ���������� ������� (������ �� 8 ���) � �������� �����*/
	int j;
	int i = 0;
	long N_total = 0;                   // ������ ����� � ����� ���-�� ���������
	int N_un = 0;                  // ���-�� ���������� ���������
	
	while ((j = fgetc(fp_in)) != EOF)      // ������ (������) �����
	{
		if (!symb[j])
			N_un++;
		symb[j]++; N_total++;
	}
	
	TSYM *syms = (TSYM *)malloc(N_un * sizeof(TSYM));
	
	TSYM t_syms;

	for (j = 0; j < M; j++)        // ���������� ���������
	{
		if (symb[j])
		{
			syms[i].ch = (unsigned char)j;
			syms[i].freq = (float)symb[j] / N_total;
			i++;
		}
		if (i == N)
			break;
	}

	for (i = 0; i < N_un - 1; i++)        // ���������� �� ��������
	{
		for (j = i + 1; j < N_un; j++)
		{
			if (syms[i].freq < syms[j].freq)
			{
				t_syms = syms[i];
				syms[i] = syms[j];
				syms[j] = t_syms;
			}
		}
	}
	for (int i = 0; i <N_un; i++)            // ������ �������
		printTab(&syms[i]);
	free(syms);

}
void printTab(TSYM* item)
{
	if (item != NULL)
		printf(" | ch: %c || freq: %f |\n =========================\n", item->ch, item->freq);		
}