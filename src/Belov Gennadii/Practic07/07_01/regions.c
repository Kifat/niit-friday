#include "regions.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

PITEM createList(PNAME_REC name_rec) // ������ ������(��������� ITEM) �� ����� ������\�� ������ ��������� 
									 //!��� ������ ������
									 // ������ ��� ������ � ����������
{
	PITEM item = (PITEM)malloc(sizeof(TITEM));
	item->name_rec = name_rec;  // ���������� ���������
	item->prev = NULL;
	item->next = NULL;
	return item;
}
PNAME_REC createName(char *line)   // ��������� ������ \���������� �� �������
{
	int i = 0;
	PNAME_REC rec = (PNAME_REC)malloc(sizeof(TNAME_REC));
	while (*line && *line != ',')            // ���� �� ������ ������ � �� ���������� ','
		rec->country[i++] = *line++;
	rec->country[i] = 0;              // ����� ������ � �������
	line++;                        //���������� ','
	i = 0;
	while (*line && *line != ',')       // ��������� �����������    (���� ���)
		rec->cod[i++] = *line++;
	rec->cod[i] = 0;
	line++;
	i = 0;
	while (*line)
		rec->region[i++] = *line++;          // ������ ����������
	rec->region[i - 1] = 0;   // ����� ������ �� \n
	return rec;
}
PITEM addToTail(PITEM tail,
	PNAME_REC name_rec)
{
	PITEM item = createList(name_rec); // ������� ��������� name_rec � ������� ������� ��������� ���������
									   // item � �������� �����������
	if (tail != NULL)
	{
		tail->next = item;   
		item->prev = tail;   // ������ �����
	}
	return item;
}
int countList(PITEM head)
{
	int count = 0;
	while (head)
	{
		count++;
		head = head->next;
	}
	return count;
}
PITEM findByRegion(PITEM head, char *name)
{
	while (head)
	{
		if (strcmpi(head->name_rec->region, name) == 0)
			return head;
		head = head->next;
	}
	return NULL;
}
void printRegion(PITEM item)
{
	if (item != NULL)
		printf("%s / %s / %s\n", item->name_rec->country, item->name_rec->cod, item->name_rec->region);
}
PITEM findByCountry(PITEM head, char *name)
{
	while (head)
	{
		if (strcmpi(head->name_rec->country, name) == 0)
			return head;
		head = head->next;
	}
	return NULL;
}
void printCountry(PITEM itemCoun)
{
	PITEM item = itemCoun;
	if (item != NULL)
	{
	    printf("%s /\n", item->name_rec->country);
		while (strcmpi(itemCoun->name_rec->country, item->name_rec->country) == 0)
		{
			printf("     %s / %s\n", item->name_rec->cod, item->name_rec->region);
		    item = item->next;
		}
	}
}