// ��������� "������ �����" �� 1 �� 100
#include <stdio.h>
#include <time.h>
#include <locale.h>

int main(void)
{
	setlocale(LC_CTYPE, "");

	unsigned int numberSecret, numberYour, numberTry;
	srand(time(0));
	printf("��������� ������� ����� �� 1 �� 100\n");
	numberSecret = rand() %100 + 1;    // ��������� ���������� �����
	numberYour = 0;     // ����� ������������
	numberTry = 0;      // ����� �������
	while (numberYour != numberSecret)   
	{
	  printf("�������� ���: ");
	  if (scanf("%u", &numberYour) == 1 && numberYour <= 100 && numberYour > 0)  // �������� �����
	  {
		  numberTry++;
		  if (numberYour < numberSecret) printf("������ �����������\n");
		  else if (numberYour > numberSecret) printf("������ �����������\n");
	  }
	  else break;
	} 
	
	if (numberYour==numberSecret) printf("������! �� %u �������\n", numberTry); 
	else printf("�� �� �����\n");
	return 0;
}