// ��������� ��������� ������ ����������� �����
#include <stdio.h>
#include <time.h>
#include <windows.h>

int main(void)
{
	unsigned int timeFly = 00;                      // ������ ������� � ������� ������ �����
	float height, length, heightFly;
	const float g = 9.81;

	printf("Enter height: ");
	if (scanf("%f", &height) == 1)                  // �������� �����
	{
		do
		{
			length = (g * timeFly * timeFly) / 2;   // ���������� ����������
			heightFly = height - length;            // ������ ��� ����� � ������ ������� timeFly
			if (heightFly <= 0) printf("BABAH\n");
			else
				printf("t = %02u c   h = %06.1f m\n", timeFly++, heightFly);
			 Sleep(100);
		} while (heightFly > 0);
	} 
	else printf("Error\n");
	return 0;
}