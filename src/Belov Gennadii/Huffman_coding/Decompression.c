#include "Huffman_coding.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <locale.h>
#define M 256
static int N_unJ;                       //  ���������� ���������� �������� � ������ �����
static TSYM symsJ[M];                // ��������� ��� �������� ������� ������ � ����� ��������
static TSYM *psymsJ[M];             // ��������� �� ��������� ��� ���������� ������
static TSYM *root;                 // ������ ������������ ������ �� ������� �������������
static int tail_101;               // ����� ������
static long FileSizeJ;           // ������ ��������� ����� (�� ������)
static char SignatureProgJ[] = "Kif";       // ���������
static char SignatureFile[sizeof SignatureProgJ];       // ��������� ����������� �����
static char expansion[5] = { 0 };     // ����������

void Dec_sion(char *name_File)
{
	FILE *fp_in, *fp2_101, *fp_decod;  // �������� (������), ������������� � ��������������� ���� ����� ������������
	
	int i = 0;
	while (*(name_File + i) != '.')       // i - ����� ����� ��������� �����
		i++;
	char *name_101 = malloc(strlen(name_File) + 5);
	char *name_decod = malloc(strlen(name_File) + 5);
	sprintf(name_101, "%.*s%s", i, name_File, ".101");    // ��� �������������� �����

	if ((fp_in = fopen(name_File, "rb")) == NULL)      //  �������� ���� ��� ������
	{
		puts("������ ��� �������� ��������� �����\n");
		exit(0);
	}
	if ((fp2_101 = fopen(name_101, "w+")) == NULL)  // ������������� ���� ��� ������ � ������
												   //������������������ ����� 101
	{
		puts("������ ��� �������� �������������� �����\n");
		exit(0);
	}
	read_FileC(fp_in, fp2_101);   // ������ ������� ����� (�������� ������������� ���� 101 ��� �������������)

	sprintf(name_decod, "%.*s%s", i, name_File, expansion);    // ��� ����� ��� ��������������� ������
	if ((fp_decod = fopen(name_decod, "wb")) == NULL)  // ���� ��� ������������
	{
		puts("������ ��� �������� ����� ��� ��������������� ������\n");
		exit(0);
	}
	rewind(fp2_101);      // ���������� ������� � ��������� ���������
	deCodes(root, fp2_101, fp_decod);     // ����������� � ������ ��������������� ������

	// �������� ������ ����� ������������
	printf("%s\n", ftell(fp_decod) == FileSizeJ ? "������ ��������� �������������" : "������ ���� �������");

	if (fclose(fp_in) != 0 || fclose(fp2_101) != 0 || fclose(fp_decod) != 0)    // �������� 
		puts("������ ��� �������� ������ ���������\n");

	free(name_101);
	free(name_decod);
}

static void read_FileC(FILE *fp_in, FILE *fp2_101)
{
	int i;
	
	// ������ ���������
	fread(SignatureFile, sizeof(SignatureProgJ), 1, fp_in);    // �������
	if (strcmp(SignatureFile, SignatureProgJ))
	{
		printf("������!\n"
			" ��������� ���� �� �������� ��������� ������������ ������ ����������\n");

		exit(0);
	}
	fread(&N_unJ, sizeof(int), 1, fp_in);      //  ���������� ���������� ��������

	for (i = 0; i<N_unJ; i++)                                //������� �������������
	{
		fread(&symsJ[i].ch, sizeof(symsJ[i].ch), 1, fp_in);
		fread(&symsJ[i].freq, sizeof(symsJ[i].freq), 1, fp_in);
		symsJ[i].code[0] = 0;
		symsJ[i].left = symsJ[i].right = 0;
		psymsJ[i] = symsJ + i;
	}

	fread(&tail_101, sizeof(int), 1, fp_in);        // ����� ������
	fread(&FileSizeJ, sizeof(long), 1, fp_in);     // ������ ��������� �����
	fread(expansion, sizeof(char), 4, fp_in);             // ����������

	root = buildTree(psymsJ, N_unJ);     // �������� ������ (�������� �-��� � Compression.c)

	create_101(fp_in, fp2_101);  // ���������� �������������� ����� ����� � ������������ � ��������� ��������� �����
}

// ���������� 101
static void create_101(FILE *fp_in, FILE *fp2_101)
{
	int i, j;
	unsigned char buf_un[8] = { 0 };   //������������ (����������) ���������� ����������� (1 ������ � 8 ��� ���� 101)
	while ((j = fgetc(fp_in)) != -1)
	{
		unpack(j, buf_un);    // �-��� ����������
		for (i = 0; i < 8; i++)
			fputc(buf_un[i], fp2_101);                  // ������ ���� � ������������� ����
	}
	fseek(fp2_101, 7 - tail_101, SEEK_END);
	fputc('z', fp2_101);                 // ������� ������
}

// ����������
static void unpack(char ch, unsigned char * buf_un)
{
	union CODE code;

	code.ch = ch;
	buf_un[0] = code.byte.b1 + '0';
	buf_un[1] = code.byte.b2 + '0';
	buf_un[2] = code.byte.b3 + '0';
	buf_un[3] = code.byte.b4 + '0';
	buf_un[4] = code.byte.b5 + '0';
	buf_un[5] = code.byte.b6 + '0';
	buf_un[6] = code.byte.b7 + '0';
	buf_un[7] = code.byte.b8 + '0';

}

// �������������
static void deCodes(TSYM *temp, FILE *fp2_101, FILE *fp_decod)
{
	int ch_101;
	while ((ch_101 = fgetc(fp2_101)) != 'z')    // ������������ ������ �������������� ����� 
	{
		if (temp->left == NULL && temp->right == NULL)
		{
			fputc(temp->ch, fp_decod);        // ��� ���������� ����� �� �������� �������������� ������
			temp = root;
		}

		ch_101 = ch_101 - '0';  // ��� ������� 0 ��� 1 ���������� ���� �� ������������ ������
		if (ch_101)
			temp = temp->right;
		if (!ch_101)
			temp = temp->left;

	}
}