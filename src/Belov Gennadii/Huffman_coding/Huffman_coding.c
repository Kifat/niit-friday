#include "Huffman_coding.h"
//#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>
#include <locale.h>

int main(int argc, char *argv[])
{
	clock_t time_start, time_end;
	double runtime;
	time_start = clock();

	setlocale(LC_CTYPE, "RUS");
	char *comp = "compress";
	char *decomp = "decompress";
	int way = 0;

	if (argc != 3 || ((way = strcmp(comp, argv[2])) && strcmp(decomp, argv[2])))
	{
		printf("������!\n" 
		   " ��� ���������� ������ ��������� �� ������ �������:\n"
			"  ��� ����� � ������ ���������: compress / decompress\n");
		
		exit(0);
	}
	// ���� (���������� ��� ������������)
	if (!way)
		C_sion(argv[1]);
	else
		Dec_sion(argv[1]);

	time_end = clock();
	printf(" time _ %.3lfc\n", runtime = (double)(time_end - time_start) / CLOCKS_PER_SEC);

	return 0;
}