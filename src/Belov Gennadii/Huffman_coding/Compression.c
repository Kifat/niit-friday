#include "Huffman_coding.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <locale.h>
#define M 256
static TSYM syms[M];                     // ��������� ��� �������� ������� ������ � ����� ��������
static TSYM *psyms[M];                   // ��������� �� ��������� ��� ���������� ������
static long N_total = 0;                   // ������ ����� � ����� ���-�� ���������
static int N_un = 0;                  // ���-�� ���������� ���������
static char SignatureProg[] = "Kif";       // ���������

void C_sion(char *name_File)
{
	FILE *fp_in, *fp_101, *fp_out;    // ��������, ������������� � ������ ����
	int i = 0;
	while (*(name_File + i) != '.')       // i - ����� ����� ��������� �����
		i++;
	char *name_101 = malloc(strlen(name_File) + 5);
	char *name_out = malloc(strlen(name_File) + 5);
	sprintf(name_101, "%.*s%s", i, name_File, ".101");    // ��� �������������� �����
	sprintf(name_out, "%.*s%s", i, name_File, ".rr");     // ��� ������� �����

	if ((fp_in = fopen(name_File, "rb")) == NULL)      //  �������� ���� ��� ������
	{
		puts("������ ��� �������� ��������� �����\n");
		exit(0);
	}

	Analysis(fp_in);             // ������ ��������� ����� � ���������� ���������
	makeCodes(buildTree(psyms, N_un));    // �������� ������ � ���������� ���� ��� �������� (1010...)

	if ((fp_101 = fopen(name_101, "w+")) == NULL)  // ������������� ���� ��� ������ � ������
		                                                     //������������������ ����� 101
	{
		puts("������ ��� �������� �������������� �����\n");
		exit(0);
	}
	Fill_101(fp_in, fp_101);    // ���������� �������������� ����� ����� � ������������ � ��������� ��������� �����

	
	if ((fp_out = fopen(name_out, "wb")) == NULL)    //  ���� ��� ������ ������ ������
	{
		puts("������ ��� �������� ����� ������ ������\n");
		exit(0);
	}

	creat_FileC(fp_101, fp_out, name_File + i);     // �������� ������� �����

	long FileSizeC = ftell(fp_out);                   // ������ ������� �����
	printf("����������� ������ %.3f\n", (float)N_total / FileSizeC);   // ����������� ������
	printf("������� ������ %.0f%%\n", (float)(N_total- FileSizeC) / N_total * 100);   // ������� ������

	if (fclose(fp_in) != 0 || fclose(fp_101) != 0 || fclose(fp_out) != 0)    // �������� 
		puts("������ ��� �������� ������ ���������\n");

	free(name_101);
	free(name_out);
	
}

// ������
static void Analysis(FILE *fp_in)
{
	int symb[M] = { 0 };     /*������� ������� ������������� ������� �� ������� ASCII
							�������� � ������� ����� ���-�� ���������� ������� (������ �� 8 ���) � �������� �����*/
	int max_repeat = 0;                 // ���������� ����� ���������� � �����
	int ex_symb = 0;           // ������� ������ �� ������� ASCII, ������������� � �������� �����
	int j;

	while ((j = fgetc(fp_in)) != EOF)
	{
		if (!symb[j])
			N_un++;		
		symb[j]++; N_total++;
		if (symb[j] > max_repeat)
			max_repeat = symb[j];
		if (j>ex_symb)
			ex_symb = j;
	}
	rewind(fp_in);      // ���������� ������� � ��������� ���������

	Fill_cod(max_repeat, ex_symb, symb);      // ���������� ��������� �� ���� �������� ������� ���������� ��������
}

// ���������� ���������
static void Fill_cod(int max_repeat, int ex_symb, int *psymb)
{
	int i, j;  
	int max_rSym;   //  ���������� ����� ���������� ����� max_repeat
	j = i = max_rSym = 0;
	while (1)
	{

		if (psymb[j] == max_repeat)
		{
			syms[i].ch = (unsigned char)j;
			syms[i].freq = (float)max_repeat / N_total;
			syms[i].code[0] = 0;
			syms[i].left = syms[i].right = 0;
			psyms[i] = syms + i;
			i++;
		}
		if (i == N_un)
			break;
		if (psymb[j] > max_rSym && psymb[j]<max_repeat)
			max_rSym = psymb[j];

		j++;

		if (j > ex_symb)
		{
			max_repeat = max_rSym;
			j = 0; max_rSym = 0;
		}

	}
}

// �������� ������
TSYM* buildTree(TSYM *psym[], int N)
{
	int i;
	TSYM *t;
	TSYM *t2;
	int f = 1;
	// ������ ��������� ����
	TSYM *temp = (TSYM*)malloc(sizeof(TSYM));
	// � ���� ������� ������������ ����� ������
	// ���������� � �������������� ��������� ������� psym
	temp->freq = psym[N - 2]->freq + psym[N - 1]->freq;
	// ��������� ��������� ���� � ����� ���������� ������
	temp->left = psym[N - 1];
	temp->right = psym[N - 2];
	temp->code[0] = 0;
	if (N == 2) // �� ������������ �������� ������� � �������� 1.0
		return temp;
	for (i = N - 2; i >= 0; i--)     // ����������� ���������� ���� � ������
	{
		psym[i + 1] = psym[i];
		if (temp->freq <= psym[i]->freq)
		{
			psym[i + 1] = temp;
			break;
		}
		if (!i)
			psym[0] = temp;
	}
	return buildTree(psym, N - 1);
}

// ���������� �����
static void makeCodes(TSYM *root)
{
	/* ���� �� ������ ��������� 0 ��� 1 � ��� ������� � ����������� �� �����������
	    ��� ������� ���� (��� ������ ������� ����������) ��� ������� ��� ��� ������� */
	if (root->left)
	{
		strcpy(root->left->code, root->code);
		strcat(root->left->code, "0");
		makeCodes(root->left);
	}
	if (root->right)
	{
		strcpy(root->right->code, root->code);
		strcat(root->right->code, "1");
		makeCodes(root->right);
	}
}

// ���������� 101
static void Fill_101(FILE *fp_in, FILE *fp_101)
{
	int i, j;
	while ((j = fgetc(fp_in)) != -1)
	{
		for (i = 0; i < 256; i++)

			if (syms[i].ch == (unsigned char)j) {
				fputs(syms[i].code, fp_101); // ������� ������ � �����
				break; // ��������� �����
			}
	}
}

// ������ � ���� ��� ������
static void creat_FileC(FILE *fp_101, FILE *fp_out, char *expansion)
{
	int i, j;
	unsigned char buf[8] = { 0 };      // ������ ��� �������� ���� 101 � ��������� ��� ������

	// ������������ ��������� ������� ����
	fwrite(SignatureProg, sizeof(SignatureProg), 1, fp_out);    // �������
	fwrite(&N_un, sizeof(int), 1, fp_out);      //  ���������� ���������� ��������
	i = 0;
	for (i = 0; i<N_un; i++)                                //������� �������������
	{
		fwrite(&syms[i].ch, sizeof(syms[i].ch), 1, fp_out);
		fwrite(&syms[i].freq, sizeof(syms[i].freq), 1, fp_out);
	}

	rewind(fp_101);
	int tail_101 = 0;             // ��� �������� ������
	while (fgetc(fp_101) != EOF)
		tail_101++;                 // ����� ���-�� �������� 101 � �������������  �����
	rewind(fp_101);
	tail_101 %= 8;
	fwrite(&tail_101, sizeof(int), 1, fp_out);        // ����� ������

	fwrite(&N_total, sizeof(long), 1, fp_out);     // ������ ��������� �����
	fwrite(expansion, sizeof(char), 4, fp_out);           // ����������


	// ������ ������ ������
	i = 0;
	while ((j = fgetc(fp_101)) != -1)
	{
		buf[i] = (unsigned char)j;
		if (i == 7)                          // ������ (��������) ���������� �������� (8 ��� ���� 101 � ������) 
		{
			fputc(pack(buf), fp_out);     // ������������ ����� ������ ������ (������ ���������� � �-��� pack)
			i = 0;    continue;
		}
		i++;
	}
	fputc(pack(buf), fp_out);    // �������� ������ (�������� ���� �� ������� 8)
}

// ��������
static unsigned char pack(unsigned char buf[])
{
	union CODE code;

	code.byte.b1 = buf[0] - '0';
	code.byte.b2 = buf[1] - '0';
	code.byte.b3 = buf[2] - '0';
	code.byte.b4 = buf[3] - '0';
	code.byte.b5 = buf[4] - '0';
	code.byte.b6 = buf[5] - '0';
	code.byte.b7 = buf[6] - '0';
	code.byte.b8 = buf[7] - '0';
	return code.ch;
}