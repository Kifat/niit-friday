#include <stdio.h>
struct SYM
{
	unsigned char ch;
	float freq;
	char code[256];
	struct SYM *left;
	struct SYM *right;
};
typedef struct SYM TSYM;
union CODE {
	unsigned char ch;
	struct {
		unsigned short b1 : 1;
		unsigned short b2 : 1;
		unsigned short b3 : 1;
		unsigned short b4 : 1;
		unsigned short b5 : 1;
		unsigned short b6 : 1;
		unsigned short b7 : 1;
		unsigned short b8 : 1;
	} byte;
};

void C_sion(char *name_File);    // ����������
//
static void Analysis(FILE *fp_in);
static void Fill_cod(int max_repeat, int ex_symb, int *psymb);
TSYM* buildTree(TSYM *psym[], int N);              // ��������� � � ������������
static void makeCodes(TSYM *root);
static void Fill_101(FILE *fp_in, FILE *fp_101);
static void creat_FileC(FILE *fp_101, FILE *fp_out, char *expansion);
static unsigned char pack(unsigned char buf[]);
//


void Dec_sion(char *name_File);  // ������������
//
static void read_FileC(FILE *fp_in, FILE *fp2_101);
static void create_101(FILE *fp_in, FILE *fp2_101);
static void unpack(char ch, unsigned char * buf_un);
static void deCodes(TSYM *temp, FILE *fp2_101, FILE *fp_decod);
//