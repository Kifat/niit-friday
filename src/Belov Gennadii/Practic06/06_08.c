// ��������� ��������� ������������� ��������� ��������������� ���������
#include <stdio.h>
#include <string.h>

int calc(char *expr);
char partition(char *buf, char *expr1, char *expr2);

int main()
{
	char buf[75];
	fgets(buf, 75, stdin);
	printf("= %d\n", calc(buf));
	return 0;
}

int calc(char *expr)                        // ����������
{
	
	if (expr[0] != '(')
		return atoi(expr);                    // ���������� ��������� ��������
	else
	{
		char expr1[75], expr2[75];
		switch (partition(expr, expr1, expr2))
		{
		case '+':
			return (calc(expr1) + calc(expr2));
		case '-':
			return (calc(expr1) - calc(expr2));
		case '*':
			return (calc(expr1) * calc(expr2));
		case '/':
			return (calc(expr1) / calc(expr2));
		}
	}
}

char partition(char *buf, char *expr1, char *expr2)   // ���������� ��������� �� �����
{
	char op;
	int count = 0;
	int i, j;
	int fOp = 1, fEnd = 1;
	for (i = 0, j = 0; fEnd; i++)
	{
		if (count >= 1 && fOp)
			expr1[j++] = buf[i];
		if (count && !fOp)
			expr2[j++] = buf[i];
		if (count == 1 && (buf[i] == '+' || buf[i] == '-' || buf[i] == '*' || buf[i] == '/'))
		{
			fOp = 0; op = buf[i]; 
			j = 0;
		}
		if (buf[i] == '(')
			count++;
		if (buf[i] == ')')
			count--;
		if (!count)
			fEnd=0;
	}
	return op;
}