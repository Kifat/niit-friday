// ��������� ��������� ���� �� ������� (����, �����) � ����������
#include <stdio.h>
#include <locale.h>


int main()
{
	setlocale(LC_CTYPE, "");
	
	float height;
	int ft, inch;
	const int qtInch = 12; // ���������� ������ � ����� ����
	const float qtCm = 2.54; // ���������� ����������� � ����� �����

	printf("������� ��� ���� � ������������ ������� ����'�����: ");
	if (scanf("%d'%d", &ft, &inch) == 2 && inch<12 && inch >= 0) // �������� �����
	{
		height = (inch + ft * qtInch)*qtCm; 
		printf("��� ���� = %.1f��\n", height);
	}
	else printf("������ �����\n");
	return 0;
}
